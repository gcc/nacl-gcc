/* Target definitions for GCC for NativeClient using ELF
   Copyright (C) 1988, 1991, 1995, 2000, 2001, 2002
   Free Software Foundation, Inc.

   Derived from sysv4.h written by Ron Guilmette (rfg@netcom.com).

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GCC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* These definitions modify those in i386elf.h. */

#undef TARGET_VERSION
#define TARGET_VERSION fprintf (stderr, " (NativeClient)");

/* Pass the NativeClient specific options to the assembler */
#undef  ASM_SPEC
#define ASM_SPEC \
  "%{v:-V} %{Qy:} %{!Qn:-Qy} %{n} %{T} " \
  "%{fnacl-library-mode:-nacl-library-mode} " \
  "%{fnacl-align-16:-nacl-align=4} " \
  "%{fnacl-align-32:-nacl-align=5} " \
  "%{Ym,*} %{Yd,*} %{Wa,*:%*}"

#undef	LIB_SPEC
#define LIB_SPEC \
  "%{pthread:-lpthread} \
   %{shared:-lc} \
   %{!shared:%{mieee-fp:-lieee} %{profile:-lc_p}%{!profile:-lc}} -lnacl -lsrpc"

/*
 * Set the linker emulation to be elf_nacl rather than linux.h's default
 * (elf_i386).
 */
#ifdef LINK_EMULATION
#undef LINK_EMULATION
#endif
#define LINK_EMULATION "elf_nacl"

/*
 * Because of NaCl's use of segment registers, negative offsets from gs: will
 * not work.  Hence we need to make TLS references explicitly compute the
 * tls base pointer and then indirect relative to it using the default
 * segment descriptor (DS).  That is, instead of
 *    movl gs:i@NTPOFF, %ecx
 * we use
 *   movl %gs:0, %eax
 *   movl i@NTPOFF(%eax), %ecx
 * There is a slight performance penalty for TLS accesses, but there does not
 * seem a way around it.
 */
#undef TARGET_TLS_DIRECT_SEG_REFS_DEFAULT
#define TARGET_TLS_DIRECT_SEG_REFS_DEFAULT 0

#undef TARGET_OS_CPP_BUILTINS
#define TARGET_OS_CPP_BUILTINS()			\
  do							\
    {							\
	LINUX_TARGET_OS_CPP_BUILTINS();			\
	builtin_define ("__native_client__=1");		\
  }							\
  while (0)
